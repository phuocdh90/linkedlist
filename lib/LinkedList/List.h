#pragma once

#include <Arduino.h>
#include "Node.h"

#ifndef LIST_SIZE_T
#define LIST_SIZE_T int16_t
#endif

template <typename T>
class List
{
private:
    Node<T> *mHead;
    Node<T> *mTail;
    LIST_SIZE_T mCount;
    // cache
    Node<T> *mCurrent;
    LIST_SIZE_T mCurrentIndex;
    void seek(LIST_SIZE_T);
    // cache-enabled methods, allows O(1) time when getting nearby element
    void rewind(void);
    Node<T> *find(LIST_SIZE_T index);
    List<T> *forEach(void (*callback)(Node<T> *, LIST_SIZE_T));

public:
    List(void);
    ~List(void);
    /**
     * Add item to the end of the list.
     */
    void add(T);
    /**
     * Add item to the start of the list.
     */
    void unshift(T);
    /**
     * Remove the last item of the list.
     */
    T pop();
    /**
     * Remove the first item of the list.
     */
    T shift();
    /**
     * Number of items of the list.
     */
    LIST_SIZE_T size(void);
    /**
     * Find the first occurent of the item in the list.
     */
    LIST_SIZE_T indexOf(T);

    /*========================================================================*/
    /*  Cache-enabled methods, allows O(1) time when getting nearby element   */
    /*========================================================================*/
    /**
     * Get item by index.
     */
    T get(LIST_SIZE_T index);
    /**
     * Set item at specified index.
     */
    void set(LIST_SIZE_T index, T value);
    /**
     * Remove the item at specified index.
     */
    T remove(LIST_SIZE_T index);
    /**
     * Remove specified item from the list. Only remove first item found.
     */
    void remove(T);

    /*========================================================================*/
    /*                           Functional methods                           */
    /*========================================================================*/
    /**
     * Clear list, delete all items.
     */
    List<T> *clear(void (*onDelete)(T, LIST_SIZE_T) = nullptr);
    /**
     * Loop through all items in the list.
     */
    List<T> *forEach(void (*callback)(T, LIST_SIZE_T));
    // template <typename N>
    // List<N> *map(N (*callback)(T, LIST_SIZE_T));

    /*========================================================================*/
    /*                                Utility                                 */
    /*========================================================================*/
    /**
     * Sort the list using bubble sort algorithm and custom comparer.
     */
    List<T> *sort(int8_t (*comparer)(T, T));
};

/******************************************************************/
/*                        IMPLEMENTATIONS                         */
/******************************************************************/

template <typename T>
List<T>::List(void)
{
    this->mHead = nullptr;
    this->mTail = nullptr;
    this->mCount = 0;
    this->mCurrent = nullptr;
    this->mCurrentIndex = 0;
}

template <typename T>
List<T>::~List(void)
{
    this->clear();
}

template <typename T>
void List<T>::add(T value)
{
    if (this->mTail != nullptr)
    {
        Node<T> *oldTail = this->mTail;
        this->mTail = new Node<T>(value);
        oldTail->next = this->mTail;
        this->mTail->prev = oldTail;
    }
    else
    {
        // first Node
        this->mCurrent = this->mHead = this->mTail = new Node<T>(value);
    }
    this->mCount++;
}

template <typename T>
void List<T>::unshift(T value)
{
    if (this->mHead != nullptr)
    {
        Node<T> *oldHead = this->mHead;
        this->mHead = new Node<T>(value);
        oldHead->prev = this->mHead;
        this->mHead->next = oldHead;
        // unshift() will affects cached index, while add() does not
        this->mCurrentIndex++;
    }
    else
    {
        // first Node
        this->mCurrent = this->mHead = this->mTail = new Node<T>(value);
        this->mCurrentIndex = 0;
    }
    this->mCount++;
}

template <typename T>
T List<T>::pop()
{
    return this->remove((LIST_SIZE_T)(this->mCount - 1));
}

template <typename T>
T List<T>::shift()
{
    return this->remove((LIST_SIZE_T)0);
}

template <typename T>
LIST_SIZE_T List<T>::size(void)
{
    return this->mCount;
}

template <typename T>
LIST_SIZE_T List<T>::indexOf(T item)
{
    for (LIST_SIZE_T i = 0; i < this->mCount; i++)
    {
        if (this->get(i) == item)
        {
            return i;
        }
    }
    return -1; // not found
}

template <typename T>
void List<T>::rewind(void)
{
    this->mCurrentIndex = 0;
    this->mCurrent = this->mHead;
}

template <typename T>
void List<T>::seek(LIST_SIZE_T index)
{
    while (this->mCurrentIndex > index && this->mCurrent->prev != nullptr)
    {
        this->mCurrentIndex--;
        this->mCurrent = this->mCurrent->prev;
    }
    while (this->mCurrentIndex < index && this->mCurrent->next != nullptr)
    {
        this->mCurrentIndex++;
        this->mCurrent = this->mCurrent->next;
    }
}

template <typename T>
T List<T>::get(LIST_SIZE_T index)
{
    Node<T> *node = this->find(index);
    return node ? node->value : 0; // 0 is both suitable for nullptr and zero in case of numerical types
}

template <typename T>
Node<T> *List<T>::find(LIST_SIZE_T index)
{
    if (index == 0)
    {
        this->rewind();
    }
    else
    {
        this->seek(index);
    }
    return this->mCurrent;
}

template <typename T>
void List<T>::set(LIST_SIZE_T index, T value)
{
    this->seek(index);
    if (this->mCurrentIndex == index)
    {
        // Node found
        this->mCurrent->value = value;
    }
}

template <typename T>
T List<T>::remove(LIST_SIZE_T index)
{
    T removed = 0;
    if (0 <= index && index < this->mCount)
    {
        // perform removal
        this->seek(index);
        if (this->mCurrent)
        {
            // result
            removed = this->mCurrent->value;

            // alter previous node
            if (this->mCurrent->prev)
            {
                this->mCurrent->prev->next = this->mCurrent->next;
            }
            else
            {
                // mCurrent is also mHead -> alter head
                this->mHead = this->mCurrent->next;
            }

            // alter next node
            Node<T> *next = this->mCurrent->next;
            if (next)
            {
                next->prev = this->mCurrent->prev;
            }
            else
            {
                // mCurrent is also mTail -> alter tail
                this->mTail = this->mCurrent->prev;
            }

            // update info
            this->mCount--;
            delete this->mCurrent;
            this->mCurrent = next;
            if (!this->mCurrent)
            {
                this->rewind();
            }
        }
    }
    return removed;
}

template <typename T>
void List<T>::remove(T item)
{
    LIST_SIZE_T index = this->indexOf(item);
    this->remove(index);
}

template <typename T>
List<T> *List<T>::clear(void (*onDelete)(T, LIST_SIZE_T))
{
    // delete all nodes
    Node<T> *node = this->mHead;
    LIST_SIZE_T index = 0;
    while (node != nullptr)
    {
        // allow custom cleaning of each node
        if (onDelete)
        {
            onDelete(node->value, index++);
        }
        // delete processed node and move to next node
        Node<T> *temp = node;
        node = node->next;
        delete temp;
    }
    // reset states
    this->mHead = this->mTail = this->mCurrent = nullptr;
    this->mCount = this->mCurrentIndex = 0;
    return this;
}

template <typename T>
List<T> *List<T>::forEach(void (*callback)(T, LIST_SIZE_T))
{
    Node<T> *temp = this->mHead;
    LIST_SIZE_T index = 0;
    while (temp != nullptr)
    {
        callback(temp->value, index++);
        temp = temp->next;
    }
    return this;
}

template <typename T>
List<T> *List<T>::forEach(void (*callback)(Node<T> *, LIST_SIZE_T))
{
    Node<T> *temp = this->mHead;
    LIST_SIZE_T index = 0;
    while (temp != nullptr)
    {
        callback(temp, index++);
        temp = temp->next;
    }
    return this;
}

// template <typename T>
// template <typename N>
// List<N> *List<T>::map(N (*callback)(T, LIST_SIZE_T))
// {
//     List<N> *list = new List<N>();
//     this->forEach([=](T value, LIST_SIZE_T index) -> void {
//         list->add(callback(value, index));
//     });
//     return list;
// }

template <typename T>
List<T> *List<T>::sort(int8_t (*comparer)(T, T))
{
    // simple bubble sort
    if (this->mCount <= 0)
    {
        // nothing to sort
        return this;
    }
    LIST_SIZE_T i, j;
    for (i = 0; i < this->mCount - 1; i++)
    {
        // Last i elements are already in place
        for (j = 0; j < this->mCount - i - 1; j++)
        {
            Node<T> *item1 = this->find(j);
            Node<T> *item2 = item1->next;
            if (comparer(item1->value, item2->value) > 0)
            {
                // swap(j, j + 1);
                T temp1 = item1->value;
                item1->value = item2->value;
                item2->value = temp1;
            }
        }
    }
    return this;
}
