#pragma once

#include <Arduino.h>

template <typename T>
class Node
{
private:
public:
    Node(T);
    Node(T, Node<T> *next, Node<T> *prev);
    T value;
    Node<T> *next;
    Node<T> *prev;
};

/******************************************************************/
/*                        IMPLEMENTATIONS                         */
/******************************************************************/

template <typename T>
Node<T>::Node(T value) : Node<T>(value, nullptr, nullptr)
{
}

template <typename T>
Node<T>::Node(T value, Node<T> *next, Node<T> *prev)
{
    this->value = value;
    this->prev = prev;
    this->next = next;
}
