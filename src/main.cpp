#include <Arduino.h>
#include <List.h>

List<uint8_t> list;

void listLoop(uint8_t value, LIST_SIZE_T index)
{
    Serial.print(value);
    Serial.print(", ");
}

void showList()
{
    Serial.print("[");
    list.forEach(&listLoop);
    Serial.println("]");
}

int8_t compareInt(uint8_t v1, uint8_t v2)
{
    return v1 - v2;
}

void setup()
{
    Serial.begin(9600);

    Serial.println("Add 4 3 2 1 into list");
    list.add(4);
    list.add(3);
    list.add(2);
    list.add(1);
    showList();

    Serial.println("Unshift 3 2 1 into list");
    list.unshift(3);
    list.unshift(2);
    list.unshift(1);
    showList();

    Serial.println("Set item at index 5 to 100, index 55 to 100");
    list.set(5, 100);
    list.set(55, 100);
    showList();

    Serial.print("size: ");
    Serial.println(list.size());

    Serial.print("Item at index 5: ");
    Serial.println(list.get(5));
    Serial.print("Item at index 0: ");
    Serial.println(list.get(0));

    Serial.println("Sort list");
    list.sort([](uint8_t v1, uint8_t v2) -> int8_t {
        return v1 - v2;
    });
    showList();

    Serial.println("Remove item with value = 2");
    list.remove((uint8_t)2);
    showList();

    Serial.println("Remove last item");
    list.remove((LIST_SIZE_T)(list.size() - 1));
    showList();

    Serial.println("Remove first item");
    list.remove((LIST_SIZE_T)0);
    showList();

    Serial.println("Add 6 7 and unshift 8 into list");
    list.add(6);
    list.add(7);
    list.unshift(8);
    showList();

    Serial.println("Sort list");
    list.sort(&compareInt);
    showList();

    Serial.println("Remove first item until list becomes empty");
    while (list.size() > 0)
    {
        list.remove((LIST_SIZE_T)0);
    }
    showList();

    Serial.println("Add 2 3 and unshift 10 into list");
    list.add(2);
    list.add(3);
    list.unshift(10);
    showList();

    Serial.println("Clear list");
    list.clear([](uint8_t item, LIST_SIZE_T index) -> void {
        Serial.print("Item ");
        Serial.print(item);
        Serial.println(" removed");
    });
    showList();
}

void loop()
{
    // put your main code here, to run repeatedly:
}
